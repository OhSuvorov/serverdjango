from django.urls import path
from django.views.decorators.cache import cache_page

from . import views

urlpatterns = [
    # path('', cache_page(100000)(views.handlingAnswer), name='handlingAnswer'),
    path('', (views.handlingAnswer), name='handlingAnswer'),
    path('watchCookie/', views.showOurCookie),
    path('setCookie/', views.setOwnCookie)
]
