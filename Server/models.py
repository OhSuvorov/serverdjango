import random
from http.server import BaseHTTPRequestHandler
from django.http import JsonResponse

from django.db import models


class Instance(models.Model):
    name = models.CharField('Название сущности', max_length=50)
    comment = models.TextField("Комментарий")


def __str__(self):
    return self.name


