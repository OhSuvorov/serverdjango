import random

from django.http import JsonResponse, HttpResponse
from django.views.decorators.cache import cache_page


@cache_page(90000)
def handlingAnswer(request):
    if request.method == 'GET':
        #print(showOurCookie(request))
        return JsonResponse(random.randint(1, 10) > 5, safe=False)
    if request.method == 'POST':
        information = request.body.decode("utf-8")
        information += "IT WAS ON A SERVER. HAVE A GOOD DAY!"
        return JsonResponse(information)
    else:
        return HttpResponse("NOTHING WORKS WHAT TO DO")


def showOurCookie(request):
    if "sessionid" in request.COOKIES:
        return HttpResponse("Your favorite color is %s" % \
                            request.COOKIES["sessionid"])
    else:
        return HttpResponse("You don't have a favorite color.")


def setOwnCookie(request):
    if "sessionid" in request.COOKIES:
        response = HttpResponse("Your favorite color is IN SETTING COOKIES %s" % \
                                request.COOKIES["sessionid"])
        response.set_cookie("favorite_color",
                            request.COOKIES["sessionid"])
        return response
    else:
        return HttpResponse("You didn't give a favorite color.")
